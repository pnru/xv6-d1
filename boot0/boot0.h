typedef unsigned long long uint64_t;
typedef unsigned int  uint32_t;
typedef unsigned char uint8_t;

#include "stdarg.h"

#define NULL ((void*)0)

// machine language assists (in entry.s)
//
uint64_t	r_time(void);
void		w_pmpcfg0(uint64_t x);
void		w_pmpaddr0(uint64_t x);
void		writel(void* addr, uint32_t value);
uint32_t	readl(void* addr);
void		flush_dcache_range(void* start, uint64_t len);
void		invalidate_dcache_range(void* start, uint64_t len);


// GPIO port/pin control. See section 9.7 of manual for details of permissable values.
//
typedef struct
{
	char	port;		// port number, PB=1, PC=2, etc. 
	char	pin;		// pin number 1..32
	char	mode;		// mode: 0 = input, 1 = output, 15 = disable, 2..14 = internal connection
	char	pull;		// set pull resistors, -1 is no change
	char	drv;		// set drive strength, -1 is no change
	char	data;		// set data (bool)
	char	reserved[2];
} gpio_set_t;

int set_gpio(gpio_set_t *list, int len, int set_mode);

// Clocks

void	sys_clock_init(void);
void	uart_clock_init(int);
void*	smhc_clk_init(void);

// UART
void sys_uart0_init(void);
void sys_uart_putc(char);
int  sys_uart_getc(void);

// SDCard
extern int __sd_log_level;
struct sdcard* card_mount(struct smhc *host);
struct smhc* smhc0_init(void);
int card_read_blocks(struct sdcard *card, void *dst, unsigned long start, unsigned blkcnt);
int card_write_blocks(struct sdcard *card, void *dst, unsigned long start, unsigned blkcnt);

// Utils
//
void sdelay(unsigned long);
void mdelay(uint64_t);
void *memcpy(void *, const void*, unsigned);
int stat(const char*, struct stat*);
char* strncpy(char*, const char*, int);
void* memmove(void *dst, const void *src, unsigned n);
char* strchr(const char*, char c);
int strcmp(const char*, const char*);
void fprintf(int, const char*, ...);
void printf(const char*, ...);
char* gets(char*, int max);
//uint strlen(const char*);
void* memset(void*, int, unsigned);
void* malloc(uint);
void free(void*);
int atoi(const char*);
int memcmp(const void *, const void *, unsigned);
void *memcpy(void *, const void *, unsigned);

// Machine assists
void write32(uint64_t addr, uint32_t value);
uint32_t read32(uint64_t addr);


