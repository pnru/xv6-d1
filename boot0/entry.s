	// The D1 boot rom loads this file from sector 256? of
	// the SDHC card into memory at 0x20000. It only does so
	// if this file has a correct header.
	
TEXT _entry(SB),0,$-8

	// boot0 header block, see:
	// sun20i_d1_spl/include/private_boot0.h
	// sun20i_d1_spl/include/configs/sun20iw1p1.h
	//
	JAL	R0,init		// jump to start
	WORD	$0x4e4f4765	// "eGON" } BT0 magic
	WORD	$0x3054422e	// ".BT0" }
	WORD	$0		// checksum, } filled in post-build
	WORD	$0		// length,   } by python script
	WORD	$0x00000030	// header size
	WORD	$0x30303033	// header version: "3000"
	WORD	$0x40000000	// return address
	WORD	$0x00020000	// run address
	WORD	$0		// boot CPU
	WORD	$0x00000005	// platform info 1: nand 4 bytes, sdmmc 2 bytes
	WORD	$0x00302e34	// platform info 2: "4.0"

	// set up a stack for C.
        // stack0 is declared in main.c,
        // with a 4096-byte stack
        // sp = stack0 + (hartid * 4096)
init:
	MOVW	$0,CSR(0x0304)	// = mie -> disable interrupts
	MOV	$setSB(SB),R3	// set static base
	MOV	$stack0(SB),R2
	MOV	$4096,R8
	ADD	R8,R2,R2

//	MOV	$0x400000,R8	// enable THEADISAEE extensions
//	MOV	R8,CSR(0x7c0)	// = mxstatus
	
//	MOV	$0x30013,R8	// invalidate caches, etc.
//	MOV	R8,CSR(0x7c2)	// = mcor

	MOV	$edata(SB),R8	// clear bss area
	MOV	$end(SB),R5
	MOVW	R0,0(R8)
	ADD	$4,R8,R8
	BLT	R5,R8,-2(PC)

	JAL	R0,main(SB)

	JMP	0(PC)

	//
	// assembler assists
	//

TEXT w_pmpcfg0+0(SB),0,$0
	MOVW	R8,CSR(0x03a0)
	RET

TEXT w_pmpaddr0+0(SB),0,$0
	MOVW	R8,CSR(0x03b0)
	RET

TEXT r_time+0(SB),0,$0
	MOV	CSR(0x0c01),R8
	RET
	
TEXT do_goto+0(SB),0,$0		// jump to location R8
	JALR	R0,0(R8)
	RET			// never happens

#define FENCE_IR	WORD $0x0aa0000f
#define FENCE_OW	WORD $0x0550000f

TEXT read32+0(SB),0,$0
TEXT readl+0(SB),0,$0
	MOVW	0(R8),R8
	FENCE_IR
	RET
	
TEXT write32+0(SB),0,$0
TEXT writel+0(SB),0,$0
	MOV	8(FP),R12
	FENCE_OW
	MOVW	R12,0(R8)
	RET

//
// D1 specific d-cache management functions (for DMA)
//

#define DCACHE_IPA(rs1)	WORD	$(0x02a0000b | (rs1<<15))
#define DCACHE_CPA(rs1)	WORD	$(0x0290000b | (rs1<<15))
#define SYNC_IS		WORD	$(0x01b0000b)

TEXT dcache_ipa+0(SB),0,$0
	DCACHE_IPA(8)
	RET
	
TEXT dcache_cpa+0(SB),0,$0
	DCACHE_CPA(8)
	RET

TEXT sync_is+0(SB),0,$0
	SYNC_IS
	RET

