#include "boot0.h"

// space allocated for boot0 stack
char stack0[4096];

static char buf[512];

struct smhc *host;
struct sdcard *sd;

void loadx(void);
void loadu(void);
void sdram_init(void);
void do_goto(long long);

void
main()
{
	int c, i, j;

	// set up system clocks and UART0.
	sys_clock_init();
	sys_uart0_init();
	
	// set up primary memory protection with full
	// RWX access to the first 4GB:
	w_pmpaddr0(0xffffffffULL);
	w_pmpcfg0(0x0f);
	
	printf("\nboot0 is loaded and running\n\n");

	sys_uart_getc();
	sdram_init();
	printf("\n");
	
	host = smhc0_init();
	if (!host) {
		printf("SD Card controller not initalized\n");
	}
	else {
		sd   = card_mount(host);
	}
	
	// Simple test command loop
	printf("\nType 'x' to boot xv6-d1, 'u' to boot Unix\n");
	for(;;) {
		if ((c = sys_uart_getc()) > 0) {
			switch(c) {
			case 'x': loadx(); do_goto(0x40000000); break;
			case 'u': loadu(); do_goto(0x40000000LL); break;
			default:  sys_uart_putc(c);
			}
		}
	}
}

#define XV6FIRST	(1024)
#define XV6SIZE		(2100)

void loadx(void)
{
	int i;
	char *buf;

	if (!sd) {
		printf("SD Card not mounted\n");
		return;
	}
	
	printf("\nReading xv6-d1...\n");
	
	buf = (char*)(0x40000000);
	for (i=XV6FIRST; i < (XV6FIRST + XV6SIZE); i++) {
		if (card_read_blocks(sd, buf, i, 1) != 1) {
			printf("Bad read for block %d\n", i);
			return;
		}
		buf += 512;
	}
	printf("xv6-d1 loaded at 0x40000000\n");
}

#define UNXFIRST	(4096)
#define UNXSIZE		(1024)

void loadu(void)
{
	int i;
	char *buf;

	if (!sd) {
		printf("SD Card not mounted\n");
		return;
	}
	
	printf("\nLoading Unix...\n");
	
	buf = (char*)(0x40000000);
	for (i=UNXFIRST; i < (UNXFIRST + UNXSIZE); i++) {
		if (card_read_blocks(sd, buf, i, 1) != 1) {
			printf("Bad read for block %d\n", i);
			return;
		}
		buf += 512;
	}
	printf("Unix loaded at 0x40000000\n");
}

