//
// formatted console output -- printf, panic.
//

#include "boot0.h"

typedef unsigned int uint;

static char digits[] = "0123456789abcdef";

static void
printint(int xx, int base, int sign)
{
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = xx < 0))
    x = -xx;
  else
    x = xx;

  i = 0;
  do {
    buf[i++] = digits[x % base];
  } while((x /= base) != 0);

  if(sign)
    buf[i++] = '-';

  while(--i >= 0)
    sys_uart_putc(buf[i]);
}

static void
printptr(uint64_t x)
{
  int i;
  sys_uart_putc('0');
  sys_uart_putc('x');
  for (i = 0; i < (sizeof(uint64_t) * 2); i++, x <<= 4)
    sys_uart_putc(digits[x >> (sizeof(uint64_t) * 8 - 4)]);
}

// Print to the console. only understands %d, %x, %p, %s.
void
printf(char *fmt, ...)
{
  va_list ap;
  int i, c, locking;
  char *s;

  va_start(ap, fmt);
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
    // sys_uart_putc(0, c);
    if(c != '%'){
      sys_uart_putc(c);
      continue;
    }
    c = fmt[++i] & 0xff;
    if(c == 0)
      break;
    switch(c){
    case 'd':
      printint(va_arg(ap, int), 10, 1);
      break;
    case 'x':
      printint(va_arg(ap, int), 16, 1);
      break;
    case 'u':
      printint(va_arg(ap, int), 16, 0);
      break;
    case 'p':
      printptr(va_arg(ap, uint64_t));
      break;
    case 's':
      if((s = va_arg(ap, char*)) == 0)
        s = "(null)";
      for(; *s; s++)
        sys_uart_putc(*s);
      break;
    case '%':
      sys_uart_putc('%');
      break;
    default:
      // Print unknown % sequence to draw attention.
      sys_uart_putc('%');
      sys_uart_putc(c);
      break;
    }
  }
}

