
#define MMC_VDD_165_195		0x00000080	/* VDD voltage 1.65 - 1.95 */
#define MMC_VDD_20_21		0x00000100	/* VDD voltage 2.0 ~ 2.1 */
#define MMC_VDD_21_22		0x00000200	/* VDD voltage 2.1 ~ 2.2 */
#define MMC_VDD_22_23		0x00000400	/* VDD voltage 2.2 ~ 2.3 */
#define MMC_VDD_23_24		0x00000800	/* VDD voltage 2.3 ~ 2.4 */
#define MMC_VDD_24_25		0x00001000	/* VDD voltage 2.4 ~ 2.5 */
#define MMC_VDD_25_26		0x00002000	/* VDD voltage 2.5 ~ 2.6 */
#define MMC_VDD_26_27		0x00004000	/* VDD voltage 2.6 ~ 2.7 */
#define MMC_VDD_27_28		0x00008000	/* VDD voltage 2.7 ~ 2.8 */
#define MMC_VDD_28_29		0x00010000	/* VDD voltage 2.8 ~ 2.9 */
#define MMC_VDD_29_30		0x00020000	/* VDD voltage 2.9 ~ 3.0 */
#define MMC_VDD_30_31		0x00040000	/* VDD voltage 3.0 ~ 3.1 */
#define MMC_VDD_31_32		0x00080000	/* VDD voltage 3.1 ~ 3.2 */
#define MMC_VDD_32_33		0x00100000	/* VDD voltage 3.2 ~ 3.3 */
#define MMC_VDD_33_34		0x00200000	/* VDD voltage 3.3 ~ 3.4 */
#define MMC_VDD_34_35		0x00400000	/* VDD voltage 3.4 ~ 3.5 */
#define MMC_VDD_35_36		0x00800000	/* VDD voltage 3.5 ~ 3.6 */

#define MMC_MODE_HS		(1 << 0)	/* can run at 26MHz -- DS26_SDR12 */
#define MMC_MODE_HS_52MHz	(1 << 1)	/* can run at 52MHz with SDR mode -- HSSDR52_SDR25 */
#define MMC_MODE_4BIT		(1 << 2)
#define MMC_MODE_8BIT		(1 << 3)
#define MMC_MODE_SPI		(1 << 4)
#define MMC_MODE_HC		(1 << 5)

#define SD_DATA_4BIT	0x00040000
#define SD_SWITCH_CHECK		0
#define SD_SWITCH_SWITCH	1

/* SCR definitions in different words */
#define SD_HIGHSPEED_BUSY	0x00020000
#define SD_HIGHSPEED_SUPPORTED	0x00020000

#define host_is_spi(sdhc)	((sdhc)->host_caps & MMC_MODE_SPI)

#define NO_CARD_ERR		-16 /* No SD/MMC card inserted */
#define UNUSABLE_ERR		-17 /* Unusable Card */
#define COMM_ERR		-18 /* Communications Error */
#define TIMEOUT			-19

#define MMC_1X_2X_MODE_CONTROL_REG (0x03000024)

#define DS26_SDR12		(0)
#define HSSDR52_SDR25		(1)
#define HSDDR52_DDR50		(2)

struct sdcard {
	char		name[32];
	void		*priv;
	uint32_t	voltages;
	uint32_t	version;
	uint32_t	has_init;
	uint32_t	f_min;
	uint32_t	f_max;
	uint32_t	f_max_ddr;
	uint32_t	high_capacity;
	uint32_t	bus_width;
	uint32_t	clock;
	uint32_t	card_caps;
	uint32_t	host_caps;
	uint32_t	ocr;
	uint32_t	scr[2];
	uint32_t	csd[4];
	uint32_t	cid[4];
	uint32_t	rca;
	uint32_t	tran_speed;
	uint32_t	read_bl_len;
	uint32_t	write_bl_len;
	uint32_t	erase_grp_size;
	uint64_t	capacity;
	uint32_t	b_max;
	uint32_t	lba;
	uint32_t	blksz;
	uint32_t	speed_mode;
};

int	host_send_cmd(struct sdcard *card, struct sd_cmd *cmd, struct sd_data *data);
void	host_set_ios(struct sdcard *card);
int	host_update_clk(struct sdcard *card);

#define MMC_DATA_READ		(1U<<0)
#define MMC_DATA_WRITE		(1U<<1)

#define SD_VERSION_SD	0x20000
#define SD_VERSION_2	(SD_VERSION_SD | 0x20)
#define SD_VERSION_1_0	(SD_VERSION_SD | 0x10)
#define SD_VERSION_1_10	(SD_VERSION_SD | 0x1a)

#define MMC_MODE_DDR_52MHz	(1 << 6)	/* can run at 52Mhz with DDR mode -- HSDDR52_DDR50 */

#define OCR_BUSY		0x80000000
#define OCR_HCS			0x40000000

#define MMC_STATUS_MASK		(~0x0206BF7F)
#define MMC_STATUS_RDY_FOR_DATA (1 << 8)

#define CMD_GO_IDLE_STATE		0
#define CMD_SEND_OP_COND		1
#define CMD_ALL_SEND_CID		2
#define CMD_SEND_RELATIVE_ADDR		3
#define CMD_SWITCH_FUNC			6
#define CMD_SELECT_CARD			7
#define CMD_SEND_IF_COND		8
#define CMD_SEND_CSD			9
#define CMD_SEND_CID			10
#define CMD_STOP_TRANSMISSION		12
#define CMD_SEND_STATUS			13
#define CMD_READ_SINGLE_BLOCK		17
#define CMD_READ_MULTIPLE_BLOCK		18
#define CMD_WRITE_SINGLE_BLOCK		24
#define CMD_WRITE_MULTIPLE_BLOCK	25
#define CMD_APP_CMD			55
#define CMD_SPI_READ_OCR		58

#define CMDA_SET_BUS_WIDTH		6
#define CMDA_SEND_OP_COND		41
#define CMDA_SEND_SCR			51

#define MMC_RSP_PRESENT (1 << 0)
#define MMC_RSP_136	(1 << 1)	/* 136 bit response */
#define MMC_RSP_CRC	(1 << 2)	/* expect valid crc */
#define MMC_RSP_BUSY	(1 << 3)	/* card may send busy */
#define MMC_RSP_OPCODE	(1 << 4)	/* response contains opcode */

#define MMC_RSP_NONE	(0)
#define MMC_RSP_R1	(MMC_RSP_PRESENT|MMC_RSP_CRC|MMC_RSP_OPCODE)
#define MMC_RSP_R1b	(MMC_RSP_PRESENT|MMC_RSP_CRC|MMC_RSP_OPCODE|MMC_RSP_BUSY)
#define MMC_RSP_R2	(MMC_RSP_PRESENT|MMC_RSP_136|MMC_RSP_CRC)
#define MMC_RSP_R3	(MMC_RSP_PRESENT)
#define MMC_RSP_R4	(MMC_RSP_PRESENT)
#define MMC_RSP_R5	(MMC_RSP_PRESENT|MMC_RSP_CRC|MMC_RSP_OPCODE)
#define MMC_RSP_R6	(MMC_RSP_PRESENT|MMC_RSP_CRC|MMC_RSP_OPCODE)
#define MMC_RSP_R7	(MMC_RSP_PRESENT|MMC_RSP_CRC|MMC_RSP_OPCODE)

#define __be32_to_cpu(x) \
	((uint32_t)( \
		(((uint32_t)(x) & (uint32_t)0x000000ffUL) << 24) | \
		(((uint32_t)(x) & (uint32_t)0x0000ff00UL) <<  8) | \
		(((uint32_t)(x) & (uint32_t)0x00ff0000UL) >>  8) | \
		(((uint32_t)(x) & (uint32_t)0xff000000UL) >> 24) ))

struct sd_cmd {
	unsigned cmdidx;
	unsigned resp_type;
	unsigned cmdarg;
	unsigned response[4];
	unsigned flags;
};

struct sd_data {
	union {
		char *dest;
		const char *src;/* src buffers don't get written to */
	} b;
	unsigned flags;
	unsigned blocks;
	unsigned blocksize;
};
