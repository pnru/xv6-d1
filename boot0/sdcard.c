
#include "boot0.h"
#include "sd.h"

#ifdef WITH_LOG
#define LOG(...)	if (__sd_log_level>=1) printf(__VA_ARGS__)
#else
#define LOG(...)	
#endif

static void set_bus_width(struct sdcard *card, uint32_t width)
{
	LOG("[card] set bus width\n");
	card->bus_width = width;
	host_set_ios(card);
}

static void set_clock(struct sdcard *card, uint32_t clock)
{
	LOG("[card] set clock speed\n");
	if (clock > card->f_max)
		clock = card->f_max;

	if (clock < card->f_min)
		clock = card->f_min;

	card->clock = clock;
	host_set_ios(card);
}

static int go_idle(struct sdcard *card)
{
	struct sd_cmd cmd;
	int err;

	LOG("[card] go idle\n");
	mdelay(1);

	cmd.cmdidx    = CMD_GO_IDLE_STATE;
	cmd.cmdarg    = 0;
	cmd.resp_type = MMC_RSP_NONE;
	cmd.flags     = 0;

	err = host_send_cmd(card, &cmd, NULL);

	if (err) {
		LOG("[card] set idle failed\n");
		return err;
	}

	mdelay(2);
	return 0;
}

static int send_if_cond(struct sdcard *card)
{
	struct sd_cmd cmd;
	int err;

	LOG("[card] if cond\n");

	cmd.cmdidx = CMD_SEND_IF_COND;
	// Set the bit if the host supports voltages between 2.7 and 3.6 V
	cmd.cmdarg    = ((card->voltages & 0xff8000) != 0) << 8 | 0xaa;
	cmd.resp_type = MMC_RSP_R7;
	cmd.flags     = 0;

	err = host_send_cmd(card, &cmd, NULL);

	if (err) {
		LOG("SD0 send if cond failed\n");
		return err;
	}

	if ((cmd.response[0] & 0xff) != 0xaa)
		return UNUSABLE_ERR;
	else
		card->version = SD_VERSION_2;

	LOG("[card] if cond OK, set SD version 2\n");
	return 0;
}

static int send_op_cond(struct sdcard *card)
{
	int timeout = 1000;
	int err;
	struct sd_cmd cmd;

	LOG("[card] op cond\n");
	do {
		cmd.cmdidx    = CMD_APP_CMD;
		cmd.resp_type = MMC_RSP_R1;
		cmd.cmdarg    = 0;
		cmd.flags     = 0;

		err = host_send_cmd(card, &cmd, NULL);
		if (err) {
			LOG("[card] op cond - app cmd failed\n");
			return err;
		}

		cmd.cmdidx    = CMDA_SEND_OP_COND;
		cmd.resp_type = MMC_RSP_R3;

		/* As per Allwinner SDK:
		 * Most cards do not answer if some reserved bits
		 * in the ocr are set. However, Some controller
		 * can set bit 7 (reserved for low voltages), but
		 * how to manage low voltages SD card is not yet
		 * specified.
		 */
		cmd.cmdarg = host_is_spi(card) ? 0 : (card->voltages & 0xff8000);

		if (card->version == SD_VERSION_2)
			cmd.cmdarg |= OCR_HCS;

		err = host_send_cmd(card, &cmd, NULL);
		if (err) {
			LOG("[card] send op cond cmd failed\n");
			return err;
		}

		mdelay(1);

	} while ((!(cmd.response[0] & OCR_BUSY)) && timeout--);

	if (timeout <= 0) {
		LOG("[card] op cond - wait card init failed\n");
		return UNUSABLE_ERR;
	}

	if (card->version != SD_VERSION_2)
		card->version = SD_VERSION_1_0;

	if (host_is_spi(card)) { /* read OCR for spi */
		cmd.cmdidx    = CMD_SPI_READ_OCR;
		cmd.resp_type = MMC_RSP_R3;
		cmd.cmdarg    = 0;
		cmd.flags     = 0;

		err = host_send_cmd(card, &cmd, NULL);

		if (err) {
			LOG("[card] op cond - spi read ocr failed\n");
			return err;
		}
	}
	card->ocr = cmd.response[0];

	card->high_capacity = ((card->ocr & OCR_HCS) == OCR_HCS);
	card->rca = 0;

	LOG("[card] op cond - SD version %u, HC = %d\n", card->version, card->high_capacity);
	return 0;
}

static int wait_ready(struct sdcard *card, int timeout)
{
	struct sd_cmd cmd;
	int err;

	cmd.cmdidx    = CMD_SEND_STATUS;
	cmd.resp_type = MMC_RSP_R1;
	cmd.cmdarg    = card->rca << 16;
	cmd.flags     = 0;

	do {
		err = host_send_cmd(card, &cmd, NULL);
		if (err) {
			LOG("[card] Send status failed\n");
			return err;
		}
		else if (cmd.response[0] & MMC_STATUS_RDY_FOR_DATA)
			break;

		mdelay(1);

		if (cmd.response[0] & MMC_STATUS_MASK) {
			LOG("[card] Status Error: 0x%08X\n", cmd.response[0]);
			return COMM_ERR;
		}
	} while (timeout--);

	if (!timeout) {
		LOG("[card] Timeout waiting card ready\n");
		return TIMEOUT;
	}
	return 0;
}

static int sd_switch(struct sdcard *card, int mode, int group, uint8_t value, uint8_t *resp)
{
	struct sd_cmd cmd;
	struct sd_data data;

	/* Switch the frequency */
	cmd.cmdidx    = CMD_SWITCH_FUNC;
	cmd.resp_type = MMC_RSP_R1;
	cmd.cmdarg    = ((uint32_t)mode << 31) | 0xffffff;
	cmd.cmdarg   &= ~(0xf << (group * 4));
	cmd.cmdarg   |= value << (group * 4);
	cmd.flags     = 0;

	data.b.dest    = (char *)resp;
	data.blocksize = 64;
	data.blocks    = 1;
	data.flags     = MMC_DATA_READ;

	return host_send_cmd(card, &cmd, &data);
}

static int change_freq(struct sdcard *card)
{
	struct sd_cmd	cmd;
	struct sd_data	data;
	uint32_t	scr[2];
	uint32_t	switch_status[16];
	int		err;
	int		timeout;

	LOG("[card] in change_freq\n");

	card->card_caps = 0;

	if (host_is_spi(card))
		return 0;

	/* Read the SCR to find out if this card supports higher speeds */
	cmd.cmdidx    = CMD_APP_CMD;
	cmd.resp_type = MMC_RSP_R1;
	cmd.cmdarg    = card->rca << 16;
	cmd.flags     = 0;

	err = host_send_cmd(card, &cmd, NULL);

	if (err) {
		LOG("[card change_freq - app cmd failed\n");
		return err;
	}

	cmd.cmdidx    = CMDA_SEND_SCR;
	cmd.resp_type = MMC_RSP_R1;
	cmd.cmdarg    = 0;
	cmd.flags     = 0;

	timeout = 3;

retry_scr:
	data.b.dest    = (char *)&scr;
	data.blocksize = 8;
	data.blocks    = 1;
	data.flags     = MMC_DATA_READ;

	err = host_send_cmd(card, &cmd, &data);

	if (err) {
		if (timeout--)
			goto retry_scr;

		LOG("[card] change_freq - send scr failed\n");
		return err;
	}

	card->scr[0] = __be32_to_cpu(scr[0]);
	card->scr[1] = __be32_to_cpu(scr[1]);
	
	LOG("[card] change_freq - scr0=%u scr1=%u\n");

	switch ((card->scr[0] >> 24) & 0x0f) {
	case 0:  card->version = SD_VERSION_1_0;	break;
	case 1:  card->version = SD_VERSION_1_10;	break;
	case 2:  card->version = SD_VERSION_2;		break;
	default: card->version = SD_VERSION_1_0;
	}

	if (card->scr[0] & SD_DATA_4BIT)
		card->card_caps |= MMC_MODE_4BIT;

	/* Version 1.0 doesn't support switching */
	if (card->version == SD_VERSION_1_0)
		return 0;

	LOG("[card] change_freq - start switch\n");

	timeout = 4;
	while (timeout--) {
		err = sd_switch(card, SD_SWITCH_CHECK, 0, 1, (uint8_t *)&switch_status);
		if (err) {
			LOG("[card] change_freq - heck high speed status faild\n");
			return err;
		}

		/* The high-speed function is busy.  Try again */
		if (!(__be32_to_cpu(switch_status[7]) & SD_HIGHSPEED_BUSY))
			break;
	}

	/* If high-speed isn't supported, we return */
	if (!(__be32_to_cpu(switch_status[3]) & SD_HIGHSPEED_SUPPORTED))
		return 0;

	err = sd_switch(card, SD_SWITCH_SWITCH, 0, 1, (uint8_t *)&switch_status);
	if (err) {
		LOG("[card] change_freq - switch to high speed failed\n");
		return err;
	}

	err = host_update_clk(card);
	if (err) {
		LOG("[card] update clock failed after send cmd6 to switch to sd high speed mode\n");
		return err;
	}

	if ((__be32_to_cpu(switch_status[4]) & 0x0f000000) == 0x01000000) {
		card->card_caps |= MMC_MODE_HS;
		card->speed_mode = HSSDR52_SDR25;
	}
	LOG("[card] change_freq - mode now %u, speed now %u\n", card->card_caps, card->speed_mode);

	return 0;
}

//
// Startup sequence to identify the card and set the fastest transport
// speed supported by the host-card combination. See:
// "SD Specifications, Part 1, Physical Layer Specification" 
// Chapter 4 and Chapter 7 for details. Also based on the Allwinner SDK.
//
static int startup(struct sdcard *card)
{
	int 		err;
	uint32_t	mult, freq;
	uint64_t	cmult, csize, capacity;
	struct sd_cmd	cmd;
	char		ext_csd[512];
	int 		timeout  = 1000;

	LOG("[card] in card startup\n");

	/* Put the Card in Identify Mode */
	cmd.cmdidx = host_is_spi(card) ? CMD_SEND_CID
				       : CMD_ALL_SEND_CID; /* cmd not supported in spi */
	cmd.resp_type = MMC_RSP_R2;
	cmd.cmdarg    = 0;
	cmd.flags     = 0;

	err = host_send_cmd(card, &cmd, NULL);

	if (err) {
		LOG("[card] startup - Card Identify Mode failed\n");
		return err;
	}

	memcpy(card->cid, cmd.response, 16);

	// Get the Relatvie Address.
	// This also puts SD cards into Standby State
	if (!host_is_spi(card)) { /* cmd not supported in spi */
		cmd.cmdidx    = CMD_SEND_RELATIVE_ADDR;
		cmd.cmdarg    = card->rca << 16;
		cmd.resp_type = MMC_RSP_R6;
		cmd.flags     = 0;

		err = host_send_cmd(card, &cmd, NULL);
		if (err) {
			LOG("[card] startup - send relative addr failed\n");
			return err;
		}
		card->rca = (cmd.response[0] >> 16) & 0xffff;
	}

	/* Get the Card-Specific Data */
	cmd.cmdidx    = CMD_SEND_CSD;
	cmd.resp_type = MMC_RSP_R2;
	cmd.cmdarg    = card->rca << 16;
	cmd.flags     = 0;

	err = host_send_cmd(card, &cmd, NULL);

	/* Waiting for the ready status */
	wait_ready(card, timeout);

	if (err) {
		LOG("[card] startup - get csd failed\n");
		return err;
	}

	card->csd[0] = cmd.response[0];
	card->csd[1] = cmd.response[1];
	card->csd[2] = cmd.response[2];
	card->csd[3] = cmd.response[3];

	// Multiplier values for TRAN_SPEED, scaled x 10
	static const int multipliers[] = {
		0, /* reserved */
		10, 12, 13, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 70, 80,
	};
	
	// Divider values for TRAN_SPEED, scaled / 10
	static const int fbase[] = {
		10000, 100000, 1000000, 10000000,
	};

	freq = fbase[(cmd.response[0] & 0x7)];
	mult = multipliers[((cmd.response[0] >> 3) & 0xf)];

	card->tran_speed = freq * mult;

	card->read_bl_len  = 1 << ((cmd.response[1] >> 16) & 0xf);
	card->write_bl_len = card->read_bl_len;

	if (card->high_capacity) {
		csize = (card->csd[1] & 0x3f) << 16 |
			(card->csd[2] & 0xffff0000) >> 16;
		cmult = 8;
	} else {
		csize = (card->csd[1] & 0x3ff) << 2 |
			(card->csd[2] & 0xc0000000) >> 30;
		cmult = (card->csd[2] & 0x00038000) >> 15;
	}

	card->capacity  = (csize + 1) << (cmult + 2);
	card->capacity *= card->read_bl_len;

	if (card->read_bl_len > 512)
		card->read_bl_len = 512;

	if (card->write_bl_len > 512)
		card->write_bl_len = 512;

	/* Select the card, and put it into Transfer Mode */
	if (!host_is_spi(card)) { /* cmd not supported in spi */
		cmd.cmdidx    = CMD_SELECT_CARD;
		cmd.resp_type = MMC_RSP_R1b;
		cmd.cmdarg    = card->rca << 16;
		cmd.flags     = 0;

		err = host_send_cmd(card, &cmd, NULL);
		if (err) {
			LOG("[card] startup - card selection failed\n");
			return err;
		}
	}

	set_clock(card, 25000000);
	
	//For SD, the erase group is always one sector
	card->erase_grp_size = 1;

	err = change_freq(card);
	if (err) {
		LOG("[card] startup - change speed mode failed\n");
		return err;
	}

	if (card->card_caps & MMC_MODE_4BIT) {
		cmd.cmdidx    = CMD_APP_CMD;
		cmd.resp_type = MMC_RSP_R1;
		cmd.cmdarg    = card->rca << 16;
		cmd.flags     = 0;

		err = host_send_cmd(card, &cmd, NULL);
		if (err) {
			LOG("[card] startup - send app cmd failed\n");
			return err;
		}

		cmd.cmdidx    = CMDA_SET_BUS_WIDTH;
		cmd.resp_type = MMC_RSP_R1;
		cmd.cmdarg    = 2;
		cmd.flags     = 0;

		err = host_send_cmd(card, &cmd, NULL);
		if (err) {
			LOG("[card] startup - set bus width failed\n");
			return err;
		}
		set_bus_width(card, 4);
	}

	if (card->card_caps & MMC_MODE_HS)
		card->tran_speed = 50000000;
	else
		card->tran_speed = 25000000;

	set_clock(card, card->tran_speed);

	/* fill in device description */
	card->blksz = card->read_bl_len;
	card->lba   = card->capacity >> 9;
	
	static char *spd_name[] = {
		"DS26/SDR12", "HSSDR52/SDR25", "HSDDR52/DDR50",
		"HS200/SDR104", "HS400"
	};
	printf("Found card:\n");
	printf("%s %d bit\n", spd_name[card->speed_mode], card->bus_width);
	printf("%d Hz\n", card->clock);
	printf("%d MB\n", card->lba >> 11);
	return 0;
}

//
// Mount card and perform card startup routine. This leaves the card
// ready for data reads and writes.
//
struct sdcard* card_mount(struct smhc *host)
{
	struct sdcard *card;
	int err;

	static struct sdcard card0;

	if (host == NULL) {
		LOG("Host not initialized\n");
		return NULL;
	}
	if (card0.has_init) {
		LOG("A card is already mounted\n");
		return NULL;
	}

	// Set up card structure
	//
	memset(&card0,  0, sizeof(struct sdcard));
	card = &card0;
	strncpy(card->name, "SMHC0 SD", 31);
	card->priv	   = host;
	
	card->voltages  = MMC_VDD_29_30 | MMC_VDD_30_31 | MMC_VDD_31_32 |
			  MMC_VDD_32_33 | MMC_VDD_33_34 | MMC_VDD_34_35 |
			  MMC_VDD_35_36;
			 
	card->host_caps = MMC_MODE_HS_52MHz | MMC_MODE_HS | MMC_MODE_HC |
			  MMC_MODE_4BIT;
			  
	card->b_max	= 1024;
	
	card->f_min     =   400000; // 400 kHz
	card->f_max     = 50000000; //  50 MHz
	card->f_max_ddr = 50000000; //  50 Mhz

	set_bus_width(card, 1);
	set_clock(card, 1);

	/* Reset the card */
	err = go_idle(card);
	if (err) {
		LOG("[card] card reset failed\n");
		return NULL;
	}

	printf("Mounting card\n");
	
	/* Test for SD version 2 */
	err = send_if_cond(card);

	/* Now try to get the SD card's operating condition */
	err = send_op_cond(card);
	if (err) {
		LOG("[card] init - card did not respond to voltage select\n");
		LOG("[card] fatal init error\n");
		return NULL;
	}
	
	err = startup(card);
	if (err) {
		LOG("[card] card init error\n");
		card->has_init = 0;
		return NULL;
	} else {
		card->has_init = 1;
		LOG("[card] card init OK\n");
		return card;
	}
}

//
// Read block(s) from card
//

int card_read_blocks(struct sdcard *card, void *dst, unsigned long start,
		    unsigned blkcnt)
{
	struct sd_cmd cmd;
	struct sd_data data;
	int timeout = 1000;

	if (blkcnt > 1)
		cmd.cmdidx = CMD_READ_MULTIPLE_BLOCK;
	else
		cmd.cmdidx = CMD_READ_SINGLE_BLOCK;

	if (card->high_capacity)
		cmd.cmdarg = start;
	else
		cmd.cmdarg = start * card->read_bl_len;

	cmd.resp_type = MMC_RSP_R1;
	cmd.flags     = 0;

	data.b.dest    = dst;
	data.blocks    = blkcnt;
	data.blocksize = card->read_bl_len;
	data.flags     = MMC_DATA_READ;

	if (host_send_cmd(card, &cmd, &data)) {
		LOG("[card] read block failed\n");
		return 0;
	}

	if (blkcnt > 1) {
		cmd.cmdidx    = CMD_STOP_TRANSMISSION;
		cmd.cmdarg    = 0;
		cmd.resp_type = MMC_RSP_R1b;
		cmd.flags     = 0;
		if (host_send_cmd(card, &cmd, NULL)) {
			LOG("[card] read block - fail to send stop cmd\n");
			return 0;
		}

		/* Waiting for the ready status */
		wait_ready(card, timeout);
	}

	return blkcnt;
}

//
// Write block(s) to card
//
int card_write_blocks(struct sdcard *card, void *dst, unsigned long start,
		    unsigned blkcnt)
{
	struct sd_cmd	cmd;
	struct sd_data	data;
	int		timeout = 1000;

	if (blkcnt > 1)
		cmd.cmdidx = CMD_WRITE_MULTIPLE_BLOCK;
	else
		cmd.cmdidx = CMD_WRITE_SINGLE_BLOCK;

	if (card->high_capacity)
		cmd.cmdarg = start;
	else
		cmd.cmdarg = start * card->read_bl_len;

	cmd.resp_type = MMC_RSP_R1;
	cmd.flags     = 0;

	data.b.dest    = dst;
	data.blocks    = blkcnt;
	data.blocksize = card->read_bl_len;
	data.flags     = MMC_DATA_WRITE;

	if (host_send_cmd(card, &cmd, &data)) {
		LOG("[card] write block failed\n");
		return 0;
	}

	if (blkcnt > 1) {
		cmd.cmdidx    = CMD_STOP_TRANSMISSION;
		cmd.cmdarg    = 0;
		cmd.resp_type = MMC_RSP_R1b;
		cmd.flags     = 0;
		if (host_send_cmd(card, &cmd, NULL)) {
			LOG("[card] write block - fail to send stop cmd\n");
			return 0;
		}

		/* Waiting for the ready status */
		wait_ready(card, timeout);
	}

	return blkcnt;
}
