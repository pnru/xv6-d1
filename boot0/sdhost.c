
#include "boot0.h"
#include "sd.h"

// log level 0 = no logging, 1 = card level, 2 = card + host level
//
int __sd_log_level = 2;

#ifdef WITH_LOG
#define LOG(...)	if (__sd_log_level>=2) printf(__VA_ARGS__)
#else
#define LOG(...)	
#endif

//
// Interface the the SMHC memory card controller of the D1 chip.
// See D1 User Manual Chapter 7 for usage.
//
// This driver is meant for the Nezha board, with a MicroSD card
// connected to SDHC0.
//

#define SMHC0_BASE	(0x04020000)

// SMHC0 controller registers. See section 7.5.2 of D1 manual.
//
struct regs {
	uint32_t gctrl;		// (0x00) SMC Global Control Register
	uint32_t clkcr;		// (0x04) SMC Clock Control Register
	uint32_t timeout;	// (0x08) SMC Time Out Register
	uint32_t width;		// (0x0C) SMC Bus Width Register
	uint32_t blksz;		// (0x10) SMC Block Size Register
	uint32_t bytecnt;	// (0x14) SMC Byte Count Register
	uint32_t cmd;		// (0x18) SMC Command Register
	uint32_t arg;		// (0x1C) SMC Argument Register
	uint32_t resp0;		// (0x20) SMC Response Register 0
	uint32_t resp1;		// (0x24) SMC Response Register 1
	uint32_t resp2;		// (0x28) SMC Response Register 2
	uint32_t resp3;		// (0x2C) SMC Response Register 3
	uint32_t imask;		// (0x30) SMC Interrupt Mask Register
	uint32_t mint;		// (0x34) SMC Masked Interrupt Status Register
	uint32_t rint;		// (0x38) SMC Raw Interrupt Status Register
	uint32_t status;	// (0x3C) SMC Status Register
	uint32_t ftrglevel;	// (0x40) SMC FIFO Threshold Watermark Register
	uint32_t funcsel;	// (0x44) SMC Function Select Register
	uint32_t cbcr;		// (0x48) SMC CIU Byte Count Register
	uint32_t bbcr;		// (0x4C) SMC BIU Byte Count Register
	uint32_t dbgc;		// (0x50) SMC Debug Enable Register
	uint32_t csdc;		// (0x54) CRC status detect control register
	uint32_t a12a;		// (0x58) Auto command 12 argument
	uint32_t ntsr;		// (0x5c) SMC2 Newtiming Set Register
	uint32_t res1[6];	// (0x54~0x74)
	uint32_t hwrst;		// (0x78) SMC eMMC Hardware Reset Register
	uint32_t res2;		// (0x7c)
	uint32_t dmac;		// (0x80) SMC IDMAC Control Register
	uint32_t dlba;		// (0x84) SMC IDMAC Descriptor List Base Address Register
	uint32_t idst;		// (0x88) SMC IDMAC Status Register
	uint32_t idie;		// (0x8C) SMC IDMAC Interrupt Enable Register
	uint32_t chda;		// (0x90)
	uint32_t cbda;		// (0x94)
	uint32_t res3[26];	// (0x98~0xff)
	uint32_t thldc;		// (0x100) Card Threshold Control Register
	uint32_t sfc;		// (0x104  SMC Sample FIFO Control Register
	uint32_t res4[1];	// (0x105-0x10b)
	uint32_t dsbd;		// (0x10c) eMMC4.5 DDR Start Bit Detection Control
	uint32_t res5[12];	// (0x110~0x13c)
	uint32_t drv_dl;	// (0x140) drive delay control register
	uint32_t samp_dl;	// (0x144) sample delay control register
	uint32_t ds_dl;		// (0x148) data strobe delay control register
	uint32_t res6[45];	// (0x149~0x1ff)
	uint32_t fifo;		// (0x200) SMC FIFO Access Address
};

struct smhc {
	struct regs	*reg;		// pointer to host register bank
	void*		mclkbase;	// pointer to CCU clock register for SMHC0
	uint32_t	fatal_err;	// a fatal error occured, stop accepting commands
	struct dma_des* pdes;		// pointer to DMA descriptor chain
	uint32_t	mod_clk;	// host clock frequency (24MHz)
	uint32_t	clock;		// actual card clock speed

};

static uint64_t timer_get_us(void)
{
	return r_time() / 24;
}

int host_update_clk(struct sdcard *card)
{
	struct smhc *smhc = (struct smhc *)card->priv;
	
	uint32_t cmd;
	uint32_t timeout = timer_get_us() + 0xfffff;

	writel(&smhc->reg->clkcr, readl(&smhc->reg->clkcr) | (0x1U << 31));

	cmd = (1U << 31) | (1 << 21) | (1 << 13);
	writel(&smhc->reg->cmd, cmd);
	while ((readl(&smhc->reg->cmd) & 0x80000000) && (timer_get_us() < timeout)) {;}

	if (readl(&smhc->reg->cmd) & 0x80000000) {
		LOG("[host] update clk failed\n");
		return -1;
	}

	writel(&smhc->reg->clkcr, readl(&smhc->reg->clkcr) & ~(0x1U << 31));
	writel(&smhc->reg->rint,  readl(&smhc->reg->rint));

	return 0;
}

static int config_clock_modex(struct smhc *smhc, unsigned clk)
{
	unsigned div, sclk = smhc->mod_clk;
	unsigned rval      = 0;

	LOG("[host] in config_clock_modex\n");
	
	div  = (2 * sclk + clk) / (2 * clk);
	rval = readl(&smhc->reg->clkcr) & ~0xff;

	if (clk <= 400000) {
		div = 0;
		writel(&smhc->reg->drv_dl,
		       readl(&smhc->reg->drv_dl) & ~(0x1 << 7));
	} else {
		writel(&smhc->reg->drv_dl,
		       readl(&smhc->reg->drv_dl) | (0x1 << 7) | (0x3 << 16));
	}

	rval |= div >> 1;
	writel(&smhc->reg->clkcr, rval);

	rval = readl(&smhc->reg->ntsr);
	if (readl((void*)MMC_1X_2X_MODE_CONTROL_REG) & (0x1 << 3) ) {
		rval |= (1U << 31);
	} else {
		rval &= ~(1U << 31);
	}
	writel(&smhc->reg->ntsr, rval);

	LOG("[host] ntsr 0x%x, ckcr 0x%x\n",
		readl(&smhc->reg->ntsr),
		readl(&smhc->reg->clkcr));

	return 0;
}

static int config_clock(struct sdcard *card, unsigned clk)
{
	struct smhc *smhc = (struct smhc *)card->priv;
	unsigned rval = 0;

	LOG("[host] in config_clock\n");

	if (card->speed_mode == HSDDR52_DDR50) {
		if (clk > card->f_max_ddr)
			clk = card->f_max_ddr;
	}

	// Disable card clock
	rval = readl(&smhc->reg->clkcr);
	rval &= ~(1 << 16);
	writel(&smhc->reg->clkcr, rval);
	if (host_update_clk(card))
		return -1;

	config_clock_modex(smhc, clk);

	// Re-enable card clock
	rval = readl(&smhc->reg->clkcr);
	rval |= (0x1 << 16);
	writel(&smhc->reg->clkcr, rval);
	if (host_update_clk(card)) {
		LOG("[host] re-enable clock failed\n");
		return -1;
	}

	return 0;
}

static void set_ddr_mode(struct smhc *smhc, int on)
{
	uint32_t rval;

	rval = readl(&smhc->reg->gctrl);
	rval &= (~(1U << 10));

	/*  disable CCU clock */
	writel(smhc->mclkbase, readl(smhc->mclkbase) & ~(1U << 31));

	if (on) {
		rval |= (1U << 10);
	}
	writel(&smhc->reg->gctrl, rval);
	LOG("[host] ddr mode: %s\n", on ? "enabled" : "disabled");

	/*  enable CCU clock */
	writel(smhc->mclkbase, readl(smhc->mclkbase) | (1U << 31));
}

void host_set_ios(struct sdcard *card)
{
	struct smhc *smhc = (struct smhc*)card->priv;
	int flag;

	LOG("[host] set_ios width: %d, clock: %d\n", card->bus_width, card->clock);

	// Set clock speed
	if (card->clock && config_clock(card, card->clock)) {
		LOG("[host] update clock failed\n");
		smhc->fatal_err = 1;
		return;
	}
	
	// Set bus width. Only 4 and 1 supported
	flag = (card->bus_width == 4) ? 1 : 0;
	writel(&smhc->reg->width, flag);

	/* Set DDR mode */
	flag = (card->speed_mode == HSDDR52_DDR50) ? 1 : 0;
	set_ddr_mode(smhc, flag);
}

//
// Init SD/(e)MMC card controller for SD cards only (SMHC0)
//

struct smhc* smhc0_init(void)
{
	struct sdcard *card;
	struct smhc *smhc;
	uint32_t rval = 0;
	uint32_t timeout;
	int ret;
	
	static struct smhc smhc0;

	printf("Initializing SDHC0 controller\n");

	// Set up smhc structure
	//
	smhc = &smhc0;
	memset(smhc, 0, sizeof(struct smhc));
	smhc->pdes	= (struct dma_des *) (0x2C000);	//DMAC_DES_BASE_IN_SDRAM;
	smhc->reg	= (struct regs*) SMHC0_BASE;
	smhc->mod_clk 	= 24000000;
	
	// Start clock & remember clock register address (for r_op)
	smhc->mclkbase = smhc_clk_init();
	
	// Set GPIO fabric
	static gpio_set_t gpio_smhc0[] = {
		{6, 2, 2, 1, 2, -1, {0} },	// PF2 = SMC0 - CLK
		{6, 3, 2, 1, 2, -1, {0} },	// PF3 = SMC0 - CMD
		{6, 1, 2, 1, 2, -1, {0} },	// PF1 = SMC0 - D0
		{6, 0, 2, 1, 2, -1, {0} },	// PF0 = SMC0 - D1
		{6, 5, 2, 1, 2, -1, {0} },	// PF5 = SMC0 - D2
		{6, 4, 2, 1, 2, -1, {0} },	// PF4 = SMC0 - D3
	};
	set_gpio(gpio_smhc0, 6, 1);

	rval = 0;
	timeout = timer_get_us() + 0xffff;
	
	/* Reset controller */
	writel(&smhc->reg->gctrl, 0x07);
	while (readl(&smhc->reg->gctrl) & 0x07) {
		if (timer_get_us() > timeout) {
			LOG("[host] wait host reset timeout\n");
			return NULL;
		}
	}
	
#define SMC_DATA_TIMEOUT 0xffffffU
#define SMC_RESP_TIMEOUT 0xff
	
	// Set Data & Response Timeout Value
	writel(&smhc->reg->timeout, (SMC_DATA_TIMEOUT << 8) | SMC_RESP_TIMEOUT); 
	
	writel(&smhc->reg->thldc, (512 << 16) | (1U << 2) | (1U << 0));
	writel(&smhc->reg->csdc, 3);
	writel(&smhc->reg->dbgc, 0xdeb);
	
	// Enable 2x clk mode, use default clock phase
	rval = readl(&smhc->reg->ntsr);
	rval |= (1U << 31);
	writel(&smhc->reg->ntsr, rval);
	
	LOG("[host] init OK\n");
	return smhc;
}

/*
 * Read and write commands and data
 */

// Descriptor structure for the SMHC built-in DMA controller ("IDMA").
// See Section 7.2.3.9 of the D1 User Manual for details.
// The SMHC0 controller has interrupt number 56.
//
#define SDXC_DES_NUM_SHIFT 12
#define SDXC_DES_BUFFER_MAX_LEN	(1 << SDXC_DES_NUM_SHIFT)

struct dma_des {
	uint32_t flags;
	uint32_t len;
	uint32_t buf;
	uint32_t next;
};

#define DIC	0x00000002	// disable interrupt on completion
#define LAST	0x00000004	// is last descriptor
#define FIRST	0x00000008	// is first descriptor
#define CHAIN	0x00000010	// part of chain (always on)
#define EOR	0x00000020	// end-of-ring (obsolete?)
#define HASERR	0x40000000	// has error
#define OWN	0x80000000	// set = IDMA to process, reset: SMHC to process

static int setup_dma_xfer(struct smhc *host, struct sd_data *data)
{
	struct dma_des*	pdes = NULL;
	unsigned 	byte_cnt = data->blocksize * data->blocks;
	unsigned char*	buff;
	unsigned 	fragcnt;
	unsigned 	remain;
	unsigned 	i, rval;
	uint32_t	timeout = 0;

	buff = data->flags & MMC_DATA_READ ? (unsigned char *)data->b.dest :
					     (unsigned char *)data->b.src;

	// Compute the number of IDMA descriptors needed for
	// full transfer. Section 7.2.3.9
	fragcnt = byte_cnt >> SDXC_DES_NUM_SHIFT;
	remain	= byte_cnt & (SDXC_DES_BUFFER_MAX_LEN - 1);
	if (remain)
		fragcnt++;
	else
		remain = SDXC_DES_BUFFER_MAX_LEN;

	pdes = host->pdes;
	memset(pdes, 0, sizeof(struct dma_des)*fragcnt);

	// Fill out the descriptor list
	for (i = 0; i < fragcnt; i++, pdes++) {
	
		pdes->buf = ((uint64_t)buff + i * SDXC_DES_BUFFER_MAX_LEN) >> 2;
		if (i != fragcnt - 1) {
			// Generic descriptor
			pdes->flags = CHAIN|OWN|DIC;
			pdes->len   = SDXC_DES_BUFFER_MAX_LEN;
			pdes->next  = (uint64_t)(pdes + 1) >> 2;	
		}
		else {	// Last descriptor
			pdes->flags = CHAIN|OWN|LAST|EOR;
			pdes->len   = remain;
			pdes->next  = 0;
		}
		if (i == 0)
			pdes-> flags |= FIRST;

		LOG("[host] frag %u, remain %u, des%x: "
			"[0] = %x, [1] = %x, [2] = %x, [3] = %x\n",
			i, remain, (uint32_t)pdes, pdes->flags, pdes->len, pdes->buf, pdes->next);
	}

	// Flush data from cache to memory
	if (data->flags & MMC_DATA_WRITE)
		flush_dcache_range(buff, byte_cnt);
	
	// Flush IDMA descriptors from cache to memory
	flush_dcache_range(pdes, sizeof(struct dma_des) * i);
	//WR_MB();
	 
	// SMHC reset and enable DMA transfer. Section 7.2.6.1
	rval = readl(&host->reg->gctrl);
	writel(&host->reg->gctrl, rval | (1 << 5) | (1 << 2));
	timeout = timer_get_us() + 0xffff;
	while (readl(&host->reg->gctrl) & (1 << 2)) {
		if (timer_get_us() > timeout) {
			LOG("[host] wait smhc rst timeout\n");
			return -1;
		}
	}
	
	// IDMA reset, switch on and set burst mode
	writel(&host->reg->dmac, 1);
	timeout = timer_get_us() + 0xffff; // 65 ms
	while (readl(&host->reg->dmac) & 1) {
		if (timer_get_us() > timeout) {
			LOG("[host] wait idma rst timeout\n");
			return -1;
		}
	}
	writel(&host->reg->dmac, (1 << 1) | (1 << 7));
	
	// Enable TX or RX interrupt
	rval  = readl(&host->reg->idie) & (~0x03);
	rval |= (data->flags & MMC_DATA_WRITE) ? (1 << 0) : (1 << 1);
	writel(&host->reg->idie, rval);
	
	writel(&host->reg->dlba, (uint64_t)pdes >> 2);
	
	// burst-16, rx/tx trigger level=15/240
	writel(&host->reg->ftrglevel, (3U << 28) | (15 << 16) | 240); 

	return 0;
}

// Polled I/O routine to read or write the data block. Used for short
// transfers.
//
static int polled_xfer(struct smhc *host, struct sd_data *data)
{
	unsigned	i;
	unsigned	byte_cnt = data->blocksize * data->blocks;
	unsigned	*buff;
	unsigned	timeout = timer_get_us() +  0xffffff;

	if (data->flags & MMC_DATA_READ) {
		buff = (unsigned int *)data->b.dest;
		for (i = 0; i < (byte_cnt >> 2); i++) {
			while ((readl(&host->reg->status) & (1 << 2))
				   && (timer_get_us() < timeout)) {
			}
			if (readl(&host->reg->status) & (1 << 2))
				goto out;
			buff[i] = readl(&host->reg->fifo);
			timeout = timer_get_us() + 0xffffff;
		}
	} else {
		buff = (unsigned int *)data->b.src;
		for (i = 0; i < (byte_cnt >> 2); i++) {
			while ((readl(&host->reg->status) & (1 << 3))
					&& (timer_get_us() < timeout)) {
			}
			if (readl(&host->reg->status) & (1 << 3))
				goto out;
			writel(&host->reg->fifo, buff[i]);
			timeout = timer_get_us() +  0xffffff;
		}
	}

out:
	if (timer_get_us() >= timeout) {
		LOG("[ctl] data transfer by cpu failed\n");
		return -1;
	}
	return 0;
}

int host_send_cmd(struct sdcard *card, struct sd_cmd *cmd, struct sd_data *data)
{
	struct smhc *host = (struct smhc *)card->priv;
	unsigned int 	cmdval	= 0x80000000;
	unsigned int	timeout	= 0;
	int		error	= 0;
	unsigned int	status	= 0;
	unsigned int	usedma	= 0;
	unsigned int	bytecnt	= 0;

	LOG("[host] in send cmd\n");
	
	if (host->fatal_err) {
		LOG("[host] card in fatal error state\n");
		return -1;
	}
	if (cmd->resp_type & MMC_RSP_BUSY)
		LOG("[host] cmd %d check rsp busy\n", cmd->cmdidx);
		
	// Compose command word from inputs
	if (!cmd->cmdidx)			cmdval |= (1 << 15);
	if (cmd->resp_type & MMC_RSP_PRESENT)	cmdval |= (1 <<  6);
	if (cmd->resp_type & MMC_RSP_136)	cmdval |= (1 <<  7);
	if (cmd->resp_type & MMC_RSP_CRC)	cmdval |= (1 <<  8);
	
	// Set up the SMHC to transfer the data payload, if any.
	if (data) {
		if ((uint64_t)(data->b.dest) & 0x3) {
			LOG("[host] send - dest is not 4 byte aligned\n");
			error = -1;
			goto out;
		}

		cmdval |= (1 << 9) | (1 << 13);
		if (data->flags & MMC_DATA_WRITE)	cmdval |= (1 << 10);
		if (data->blocks > 1)			cmdval |= (1 << 12);

		writel(&host->reg->blksz,	data->blocksize);
		writel(&host->reg->bytecnt,	data->blocks * data->blocksize);
	}

	// For a command without data, send it direct.
	writel(&host->reg->arg,	cmd->cmdarg);
	if (!data)
		writel(&host->reg->cmd, cmdval | cmd->cmdidx);
	LOG("[host] send - cmd %d (%u), arg %u\n", cmd->cmdidx, cmdval, cmd->cmdarg);

	// For a small transfer, do the data transfer by the CPU direct,
	// using a polled loop. For a big transfer set up the IDMA unit to
	// do the actual transfer.
	if (data) {
		int ret = 0;
		
		bytecnt = data->blocksize * data->blocks;
		LOG("[host] xfer %d data bytes\n", bytecnt);

		if (0 && bytecnt > 512) {
			usedma = 1;
			writel(&host->reg->gctrl, readl(&host->reg->gctrl) & ~0x80000000);
			ret = setup_dma_xfer(host, data);
			// more logical to flush cached data here ?
			writel(&host->reg->cmd, cmdval | cmd->cmdidx);
		} else {
			writel(&host->reg->gctrl, readl(&host->reg->gctrl) |  0x80000000);
			writel(&host->reg->cmd,   cmdval | cmd->cmdidx);
			ret = polled_xfer(host, data);
		}
		if (ret) {
			LOG("[host] send - Transfer failed\n");
			error = readl(&host->reg->rint) & 0xbbc2;
			if (!error)
				error = -1;
			goto out;
		}
	}

	// Wait for the SMHC to complete the CMD and check any errors
	// See  section 7.2.6.15
	timeout = timer_get_us() + 0xffffff; // 16 sec
	do {
		status = readl(&host->reg->rint);
		if ((timer_get_us() > timeout) || (status & 0xbbc2)) {
			error = status & 0xbbc2;
			if (!error)
				error = -1;
			LOG("[host] send - cmd %d timeout, err %u\n", cmd->cmdidx, error);
			goto out;
		}
	} while (!(status & 0x04));

	if (data) {
		unsigned done = 0;
		timeout       =  timer_get_us() + (usedma ? 0xffffff : 0xffff); // 16 s or 65 ms

		// wait for the SMHC or IDMA to complete the data xfer
		do {
			status = readl(&host->reg->rint);
			if ((timer_get_us() > timeout) || (status & 0xbbc2)) {
				error = status & 0xbbc2;
				if (!error)
					error = -1;
				LOG("[host] send - data timeout, err %u\n", error);
				goto out;
			}
			done = (data->blocks > 1) ? status & (1 << 14) : status & (1 << 3);
		} while (!done);

		// If this was a IDMA read, wait for the RX interrupt to come on in
		// the status register. See section 7.2.6.27
		if ((data->flags & MMC_DATA_READ) && usedma) {
			timeout = timer_get_us() + 0xffffff; // 16 sec
			done    = 0;
			status  = 0;
			do {
				status = readl(&host->reg->idst);
				if ((timer_get_us() > timeout) || (status & 0x234)) {
					error = status & 0x1E34;
					if (!error)
						error = -1;
					LOG("[host] send - wait dma read err %x\n", error);
					goto out;
				}
				done = status & (1 << 1);

			} while (!done);
			LOG("[host] dma read finished OK\n");
		}
	}

	// Wait until the SD Card responds it is not busy anymore
	if (cmd->resp_type & MMC_RSP_BUSY) {
		timeout = timer_get_us() + 0x4ffffff; // 1 minute??
		do {
			status = readl(&host->reg->status);
			if (timer_get_us() > timeout) {
				error = -1;
				LOG("[host] busy timeout, status %u\n", status);
				goto out;
			}
		} while (status & (1 << 9));
	}
	
	// Fetch the CMD response
	if (cmd->resp_type & MMC_RSP_136) {
		cmd->response[0] = readl(&host->reg->resp3);
		cmd->response[1] = readl(&host->reg->resp2);
		cmd->response[2] = readl(&host->reg->resp1);
		cmd->response[3] = readl(&host->reg->resp0);
		LOG("[host] send - resp %u %u %u %u\n", 
		       cmd->response[3], cmd->response[2], cmd->response[1],
		       cmd->response[0]);
	} else {
		cmd->response[0] = readl(&host->reg->resp0);
		LOG("[host] send - resp %u\n", cmd->response[0]);
	}

out:
	if (data && usedma) {
		// Invalidate cache for read data area
		if (data->flags & MMC_DATA_READ)
			invalidate_dcache_range(data->b.dest, data->blocksize * data->blocks);

		// Reset all IDMA interrupt signals, reset IDMA to idle
		status = readl(&host->reg->idst);
		writel(&host->reg->idst, status);
		writel(&host->reg->idie,  0);
		writel(&host->reg->dmac,  0);
		writel(&host->reg->gctrl, readl(&host->reg->gctrl) & (~(1 << 5)) ); // 7.2.6.1
		if (data->flags & MMC_DATA_READ)
			invalidate_dcache_range(data->b.dest, data->blocksize * data->blocks);

	}

	// Reset all SMHC interrupt signals
	writel(&host->reg->rint, 0xffffffff);

	if (error) {
		writel(&host->reg->gctrl, 0x07);
		timeout = timer_get_us() + 0xffff; // 16 ms
		while (readl(&host->reg->gctrl) & 0x07) {
			if (timer_get_us() > timeout) {
				LOG("[host] send - wait ctl reset timeout2\n");
				return -1;
			}
		}
		host_update_clk(card);
		LOG("[host] send - cmd %d err %u\n", cmd->cmdidx, error);
		return -1;
	}

	LOG("[host] send - OK\n");
	return 0;
}
