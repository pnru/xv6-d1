
#include "boot0.h"
#include "clk.h"

#define UART_BASE	(0x02500000)
#define UART_SZ		(0x4000)

#define PLL_FREQ	(24000000)
#define UART_BAUD	(115200)
#define UART0		0

// Type 16650 uart registers
typedef struct
{
	uint32_t rbr;		/* 0 */
	uint32_t ier;		/* 1 */
	uint32_t fcr;		/* 2 */
	uint32_t lcr;		/* 3 */
	uint32_t mcr;		/* 4 */
	uint32_t lsr;		/* 5 */
	uint32_t msr;		/* 6 */
	uint32_t sch;		/* 7 */
} uart_t;

#define thr rbr
#define dll rbr
#define dlh ier
#define iir fcr

#define uart_lsr_thre	0x20    /* transmit-hold-register empty */
#define uart_lsr_dr	0x01    /* receiver data ready */

static uart_t *base = (uart_t *)(UART_BASE + UART0 * UART_SZ);


// Poll-wait UART0 routines
//
void sys_uart_putc(char c)
{
	while((base->lsr & uart_lsr_thre) == 0);
	base->thr = c;
}

int sys_uart_getc()
{
	return (base->lsr & uart_lsr_dr) ? base->rbr : -1;
}

// In mode 6, pin PB8 and PB9 are tied to UART0 RX and TX respectively.
//
gpio_set_t gpio_uart[] = {
	{2, 8, 6, 1, -1, -1, {0} }, /* configure PB8 to mode 6, drive 1 */
	{2, 9, 6, 1, -1, -1, {0} }, /* configure PB9 to mode 6, drive 1 */
};
#define CFGSZ (sizeof(gpio_uart)/sizeof(gpio_set_t))


// Init GPIO fabric, clock gate and UART itself
//
void sys_uart0_init(void)
{
	uint64_t addr;
	uint32_t val;
	int	 uart_clk;


	// Connect GPIO pins to UART0
	set_gpio(gpio_uart, CFGSZ, 1);

	// Reset UART0 & connect clock
	uart_clock_init(UART0);

	// Config uart registers
	uart_clk = PLL_FREQ/(16 * UART_BAUD);
	
	base->ier  = 0x00;		// disable interupts
	base->fcr  = 0xf7;		// reset fifo
	base->mcr  = 0x00;		// uart mode
	base->lcr |= 0x80;		// select divisor latch
	base->dlh  = uart_clk >> 8;	// set divisor high byte
	base->dll  = uart_clk & 0xff;	// set divisor low byte
	base->lcr &= ~0x80;		// unselect divisor latch
	base->lcr &= ~0x1f;		// set 8N1
	base->lcr |= (0<<3) | (0<<2) | (0x03);

}
