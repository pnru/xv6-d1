
#include "boot0.h"

typedef unsigned char uchar;

void*
memset(void *dst, int c, unsigned n)
{
  char *cdst = (char *) dst;
  int i;
  for(i = 0; i < n; i++){
    cdst[i] = c;
  }
  return dst;
}

int
memcmp(const void *v1, const void *v2, unsigned n)
{
  const uchar *s1, *s2;

  s1 = v1;
  s2 = v2;
  while(n-- > 0){
    if(*s1 != *s2)
      return *s1 - *s2;
    s1++, s2++;
  }

  return 0;
}

void*
memmove(void *dst, const void *src, unsigned n)
{
  const char *s;
  char *d;

  s = src;
  d = dst;
  if(s < d && s + n > d){
    s += n;
    d += n;
    while(n-- > 0)
      *--d = *--s;
  } else
    while(n-- > 0)
      *d++ = *s++;

  return dst;
}

// memcpy exists to placate GCC.  Use memmove.
void*
memcpy(void *dst, const void *src, unsigned n)
{
  return memmove(dst, src, n);
}

int
strncmp(const char *p, const char *q, unsigned n)
{
  while(n > 0 && *p && *p == *q)
    n--, p++, q++;
  if(n == 0)
    return 0;
  return (uchar)*p - (uchar)*q;
}

char*
strncpy(char *s, const char *t, int n)
{
  char *os;

  os = s;
  while(n-- > 0 && (*s++ = *t++) != 0)
    ;
  while(n-- > 0)
    *s++ = 0;
  return os;
}

// Like strncpy but guaranteed to NUL-terminate.
char*
safestrcpy(char *s, const char *t, int n)
{
  char *os;

  os = s;
  if(n <= 0)
    return os;
  while(--n > 0 && (*s++ = *t++) != 0)
    ;
  *s = 0;
  return os;
}

int
strlen(const char *s)
{
  int n;

  for(n = 0; s[n]; n++)
    ;
  return n;
}

void sdelay(unsigned long us)
{   
	uint64_t t1 = r_time();
	uint64_t t2 = t1 + us * 24;
	do {
		t1 = r_time();
	} while(t2 >= t1);
}

void mdelay(uint64_t ms)
{   
	sdelay(ms*1000);
}

// assembly assists in entry.s
void dcache_cpa(uint64_t);
void dcache_ipa(uint64_t);
void sync_is(void);

#define L1_CACHE_BYTES	64

void flush_dcache_range(void* start, uint64_t len)
{
	uint64_t i   = (uint64_t)start & ~(L1_CACHE_BYTES - 1);
	uint64_t end = i + len;
	
	// flush each cache line in range individually
	for (; i < end; i += L1_CACHE_BYTES)	dcache_cpa(i);
	sync_is();
}

void invalidate_dcache_range(void* start, uint64_t len)
{
	uint64_t i   = (uint64_t)start & ~(L1_CACHE_BYTES - 1);
	uint64_t end = i + len;
	
	// invalidate each cache line in range individually
	for (; i < end; i += L1_CACHE_BYTES)	dcache_ipa(i);
	sync_is();
}
