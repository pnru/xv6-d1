#include "common.h"


void sdelay(unsigned long us)
{
    uint64_t t1 =r_time();
    uint64_t t2 = t1 + us * 24;
    do {
        t1 = r_time();
    } while(t2 >= t1);
}

// enable device interrupts
void
intr_on()
{
	w_sstatus(r_sstatus() | SSTATUS_SIE);
}

// disable device interrupts
void
intr_off()
{
	w_sstatus(r_sstatus() & ~SSTATUS_SIE);
}

// are device interrupts enabled?
int
intr_get()
{
	uint64 x = r_sstatus();
	return (x & SSTATUS_SIE) != 0;
}
