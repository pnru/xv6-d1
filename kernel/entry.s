	// qemu -kernel loads the kernel at 0x80000000
        // and causes each CPU to jump there.
        // kernel.ld causes the following code to
        // be placed at 0x80000000.

TEXT _entry(SB),0,$-8
	// set up a stack for C.
        // stack0 is declared in start.c,
        // with a 4096-byte stack per CPU.
        // sp = stack0 + (hartid * 4096)
	MOV	$setSB(SB),R3	// set static base
	MOVW	$0,CSR(0x0304)	// mie
	MOV	$stack0(SB),R2
	MOV	$4096,R8
	MOVW	CSR(0x0f14),R9	// hartid
	ADD	$1,R9,R9
	MUL	R9,R8,R8
	ADD	R8,R2,R2
	// jump to start() in start.c
	JAL	R0,start(SB)
        // spin
	JMP	0(PC)

