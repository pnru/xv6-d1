//
// Control register access functions
//

TEXT r_plicbase+0(SB),0,$0
	MOVW	CSR(0x0fc1),R8
	RET

TEXT w_pnr+0(SB),0,$0
	//MOVW	R8,CSR(0x0101)
	RET

TEXT r_mtime+0(SB),0,$0
	MOV	CSR(0xc01),R8
	RET

// PMP Configuration

TEXT r_pmpcfg0+0(SB),0,$0
	MOVW	CSR(0x03a0),R8
	RET

TEXT w_pmpcfg0+0(SB),0,$0
	MOVW	R8,CSR(0x03a0)
	RET
	
TEXT r_pmpaddr0+0(SB),0,$0
	MOVW	CSR(0x03b0),R8
	RET

TEXT w_pmpaddr0+0(SB),0,$0
	MOVW	R8,CSR(0x03b0)
	RET
	
TEXT r_mhartid+0(SB),0,$0
	MOVW	$0,R8
	RET

// Machine Status, Machine Exception PC

TEXT r_mstatus+0(SB),0,$0
	MOVW	CSR(0x0300),R8
	RET

TEXT w_mstatus+0(SB),0,$0
	MOVW	R8,CSR(0x0300)
	RET

TEXT w_mepc+0(SB),0,$0
	MOVW	R8,CSR(0x0341)
	RET

// Supervisor Status, Supervisor Interrupt Pending

TEXT r_sstatus+0(SB),0,$0
	MOVW	CSR(0x0100),R8
	RET

TEXT w_sstatus+0(SB),0,$0
	MOVW	R8,CSR(0x0100)
	RET

TEXT r_sip+0(SB),0,$0
	MOVW	CSR(0x0144),R8
	RET

TEXT w_sip+0(SB),0,$0
	MOVW	R8,CSR(0x0144)
	RET
	
// Supervisor & Machine Interupt Enable

TEXT r_sie+0(SB),0,$0
	MOVW	CSR(0x0104),R8
	RET

TEXT w_sie+0(SB),0,$0
	MOVW	R8,CSR(0x0104)
	RET

TEXT r_mie+0(SB),0,$0
	MOVW	CSR(0x0304),R8
	RET

TEXT w_mie+0(SB),0,$0
	MOVW	R8,CSR(0x0304)
	RET

// Supervisor Exception PC

TEXT r_sepc+0(SB),0,$0
	MOVW	CSR(0x0141),R8
	RET

TEXT w_sepc+0(SB),0,$0
	MOVW	R8,CSR(0x0141)
	RET

// Machine Exception Delegation

TEXT r_medeleg+0(SB),0,$0
	MOVW	CSR(0x0302),R8
	RET

TEXT w_medeleg+0(SB),0,$0
	MOVW	R8,CSR(0x0302)
	RET

// Machine Interrupt Delegation

TEXT r_mideleg+0(SB),0,$0
	MOVW	CSR(0x0303),R8
	RET

TEXT w_mideleg+0(SB),0,$0
	MOVW	R8,CSR(0x0303)
	RET

// Supervisor Trap-Vector Base Address, low two bits are mode.

TEXT r_stvec+0(SB),0,$0
	MOVW	CSR(0x0105),R8
	RET

TEXT w_stvec+0(SB),0,$0
	MOVW	R8,CSR(0x0105)
	RET

// Machine-mode interrupt vector

TEXT w_mtvec+0(SB),0,$0
	MOVW	R8,CSR(0x0305)
	RET

// Supervisor Address Translation & Protection

TEXT r_satp+0(SB),0,$0
	MOVW	CSR(0x0180),R8
	RET

TEXT w_satp+0(SB),0,$0
	MOVW	R8,CSR(0x0180)
	RET

// Supervisor & Machine Scratch register

TEXT w_sscratch+0(SB),0,$0
	MOVW	R8,CSR(0x0140)
	RET

TEXT w_mscratch+0(SB),0,$0
	MOVW	R8,CSR(0x0340)
	RET

// Supervisor Trap Cause

TEXT r_scause+0(SB),0,$0
	MOVW	CSR(0x0142),R8
	RET

// Supervisor Trap Value

TEXT r_stval+0(SB),0,$0
	MOVW	CSR(0x0143),R8
	RET

// Machine-mode Counter-Enable

TEXT r_mcounteren+0(SB),0,$0
	MOVW	CSR(0x0306),R8
	RET

TEXT w_mcounteren+0(SB),0,$0
	MOVW	R8,CSR(0x0306)
	RET
	
// Machine-mode cycle counter

TEXT r_time+0(SB),0,$0
	MOV	CSR(0x0c01),R8
	RET

// Access the thread pointer = R18, link register = R1, global pointer = R3

TEXT r_tp+0(SB),0,$0
	MOV	R18,R8
	RET

TEXT w_tp+0(SB),0,$0
	MOV	R8,R18
	RET
	
TEXT r_ra+0(SB),0,$0
	MOV	R1,R8
	RET

TEXT r_gp+0(SB),0,$0
	MOV	R3,R8
	RET


// Do MRET from C

#define MRET		WORD $0x30200073

TEXT do_mret+0(SB),0,$0
	MRET
	
// Synchronisation functions

#define SFENCE_VMA	WORD $0x12000073
#define FENCE		WORD $0x0000000f

TEXT sfence_vma+0(SB),0,$0
	SFENCE_VMA
	RET

/*
 * The KENCC/RISCV assembler does not yet support the AMO
 * instructions. The below macro fills in the gap.
 */
#define	ADD	0x00
#define	SWAP	0x04
#define	LR	0x08
#define	SC	0x0c
#define	XOR	0x10
#define	OR	0x14
#define	AND	0x18
#define	MIN	0x1c
#define	MAX	0x20
#define	MINU	0x24
#define	MAXU	0x28

#define AQ	2	/* acquire */
#define RL	1	/* release */

#define AMOW(func_opts, rs1, rs2, rd) \
	WORD $(((func_opts)<<25)|((rs2)<<20)|((rs1)<<15)|(2<<12)|((rd)<<7)|057)

// AMO: atomically perform (rd = (rs1), (rs1) = rd func rs2)
	
TEXT __sync_lock_test_and_set+0(SB),0,$0
	MOV	16(FP),R9
	AMOW(SWAP|AQ,8,9,9)
	MOV	R9,R8
	RET

TEXT __sync_synchronize+0(SB),0,$0
	FENCE
	RET

TEXT __sync_lock_release+0(SB),0,$0
	AMOW(SWAP,8,0,0)
	RET

