// Context switch
//
//   void swtch(struct context *old, struct context *new);
//
// Save current registers in old. Load from new.	


TEXT swtch(SB),0,$0
	MOV	R1,0(R8)
	MOV	R2,8(R8)
	//MOV	R3,16(R8)
	MOV	R4,24(R8)
	MOV	R5,32(R8)
	MOV	R6,40(R8)
	MOV	R7,48(R8)

	MOV	16(R2),R9
	MOV	0(R9),R1
	MOV	8(R9),R2
	//MOV	16(R9),R3
	MOV	24(R9),R4
	MOV	32(R9),R5
	MOV	40(R9),R6
	MOV	48(R9),R7
	
	RET
