	//
        // code to switch between user and kernel space.
        //
        // this code is mapped at the same virtual address
        // (TRAMPOLINE) in user and kernel space so that
        // it continues to work when it switches page tables.
	//
	// kernel.ld causes this to be aligned
        // to a page boundary.

#define SFENCE_VMA	WORD $0x12000073
#define SRET		WORD $0x10200073

TEXT trampoline(SB),0,$0
TEXT uservec(SB),0,$0
	//
        // trap.c sets stvec to point here, so
        // traps from user space start here,
        // in supervisor mode, but with a
        // user page table.
        //
        // sscratch points to where the process's p->trapframe is
        // mapped into user space, at TRAPFRAME.
        //
        
	// swap r6 and sscratch
        // so that r6 is TRAPFRAME
	CSRRW	CSR(0x0140), R6, R6

        // save the user registers in TRAPFRAME
        MOV R1,   40(R6)
        MOV R2,   48(R6)
        MOV R3,   56(R6)
        MOV R4,   64(R6)
        MOV R5,   72(R6)
        MOV R7,   88(R6)
	MOV R8,   96(R6)
	MOV R9,  104(R6)
	MOV R10, 112(R6)
        MOV R11, 120(R6)
        MOV R12, 128(R6)
        MOV R13, 136(R6)
        MOV R14, 144(R6)
        MOV R15, 152(R6)
        MOV R16, 160(R6)
        MOV R17, 168(R6)
        MOV R18, 176(R6)
        MOV R19, 184(R6)
        MOV R20, 192(R6)
        MOV R21, 200(R6)
        MOV R22, 208(R6)
        MOV R23, 216(R6)
        MOV R24, 224(R6)
        MOV R25, 232(R6)
        MOV R26, 240(R6)
        MOV R27, 248(R6)
        MOV R28, 256(R6)
        MOV R29, 264(R6)
        MOV R30, 272(R6)
        MOV R31, 280(R6)

	// save the user a0 in p->trapframe->a0
        MOV	CSR(0x0140),R5
	MOV	R5,80(R6)

        // restore kernel stack pointer from p->trapframe->kernel_sp
        MOV	8(R6),R2

        // make tp hold the current hartid, from p->trapframe->kernel_hartid
	MOV	32(R6),R18
	
	// make gp hold the kernel global pointer (SB)
	MOV	288(R6),R3

        // load the address of usertrap(), p->trapframe->kernel_trap
	MOV 	16(R6),R5

        // restore kernel page table from p->trapframe->kernel_satp
	MOV	0(R6),R7
	MOVW	R7,CSR(0x0180)	// satp
	SFENCE_VMA

        // r6 is no longer valid, since the kernel page
        // table does not specially map p->tf.
	
        // move the 3 parameters to where kencc expects them
        MOV	R10,R8
        MOV	R11,0(R2)
	MOV	R12,8(R2)

        // jump to usertrap(), which does not return
        JMP	0(R5)

TEXT userret(SB),0,$0
        // userret(TRAPFRAME, pagetable)
        // switch from kernel to user.
        // usertrapret() calls here.
        // a0: TRAPFRAME, in user page table.
        // a1: user page table, for satp.

        // switch to the user page table.
	MOV	16(R2),R9
	MOV	R9,CSR(0x0180)	// pagetable -> satp
	SFENCE_VMA

        // put the saved user a0 in sscratch, so we
        // can swap it with our a0 (TRAPFRAME) in the last step.
        MOV	96(R8), R5
	MOV	R5, CSR(0x0140)

        // restore all but a0 from TRAPFRAME
	MOV  40(R8), R1
        MOV  48(R8), R2
        MOV  56(R8), R3
        MOV  64(R8), R4
        MOV  72(R8), R5
        MOV  80(R8), R6
        MOV  88(R8), R7
        MOV 104(R8), R9
	MOV 112(R8), R10
        MOV 120(R8), R11
        MOV 128(R8), R12
        MOV 136(R8), R13
        MOV 144(R8), R14
        MOV 152(R8), R15
        MOV 160(R8), R16
        MOV 168(R8), R17
        MOV 176(R8), R18
        MOV 184(R8), R19
        MOV 192(R8), R20
        MOV 200(R8), R21
        MOV 208(R8), R22
        MOV 216(R8), R23
        MOV 224(R8), R24
        MOV 232(R8), R25
        MOV 240(R8), R26
        MOV 248(R8), R27
        MOV 256(R8), R28
        MOV 264(R8), R29
        MOV 272(R8), R30
        MOV 280(R8), R31

	// restore user a0, and save TRAPFRAME in sscratch
	CSRRW	CSR(0x0140), R8, R8
	
        // return to user mode and user pc.
        // usertrapret() set up sstatus and sepc.
        SRET
